﻿using UnityEngine;
using System.Collections;
using UnityEditor;


[CustomPropertyDrawer(typeof(MoveScript))]


public class MovementDrawer : PropertyDrawer
{
    Waypoints waypoint;
    float extraHeight = 50.0f;

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {


        EditorGUI.BeginProperty(position, label, property);



        SerializedProperty waypointType = property.FindPropertyRelative("MoveType");
        SerializedProperty startPoint = property.FindPropertyRelative("startPoint");
        SerializedProperty endPoint = property.FindPropertyRelative("endPoint");
        SerializedProperty waitTime = property.FindPropertyRelative("waitTime");
        SerializedProperty bezierCurve = property.FindPropertyRelative("bezierCurve");
        SerializedProperty lookTarget = property.FindPropertyRelative("lookTarget");



        switch ((MoveType)waypointType.enumValueIndex)
        {
            case MoveType.STRAIGHT_LINE:
                // show waypoint.startPoint;
                Rect startRect = new Rect(position.x, position.y + 30, position.width, 18f);
                EditorGUI.PropertyField(startRect, startPoint, true);
                // show waypoint.endPoint;
                Rect endRect = new Rect(position.x, position.y + 50, position.width, 18f);
                EditorGUI.PropertyField(endRect, endPoint, true);
                //show speed
                Rect speedRect = new Rect(position.x, position.y, 70, position.height);
                EditorGUI.PropertyField(speedRect, property.FindPropertyRelative("speed"), GUIContent.none);

                break;

            case MoveType.BEZIER_CURVE:
                // show waypoint.startPoint;
                Rect bezierCurveStartRect = new Rect(position.x, position.y + 30, position.width, 18f);
                EditorGUI.PropertyField(bezierCurveStartRect, startPoint, true);
                //show curve point
                // show waypoint.endPoint;
                Rect bezierCurveEndRect = new Rect(position.x, position.y + 50, position.width, 18f);
                EditorGUI.PropertyField(bezierCurveEndRect, bezierCurve, true);
                //show speed
                Rect bezierCurveSpeedRect = new Rect(position.x, position.y, 70, position.height);
                EditorGUI.PropertyField(bezierCurveSpeedRect, property.FindPropertyRelative("speed"), GUIContent.none);
                break;

            case MoveType.WAIT:
                //how long you wait
                Rect waitRect = new Rect(position.x, position.y, 70, position.height);
                EditorGUI.PropertyField(waitRect, waitTime, GUIContent.none);
                break;

            case MoveType.LOOK_RETURN:
                // show waypoint.startPoint;
                Rect lookReturnStartRect = new Rect(position.x, position.y + 30, position.width, 18f);
                EditorGUI.PropertyField(lookReturnStartRect, startPoint, true);
                // show waypoint.endPoint;
                Rect lookReturnEndRect = new Rect(position.x, position.y + 50, position.width, 18f);
                EditorGUI.PropertyField(lookReturnEndRect, endPoint, true);
                //look point
                Rect lookReturnTargetRect = new Rect(position.x, position.y + 70, position.width, 18f);
                EditorGUI.PropertyField(lookReturnTargetRect, lookTarget, true);
                //show speed
                Rect lookReturnSpeedRect = new Rect(position.x, position.y + 90, 70, position.height);
                EditorGUI.PropertyField(lookReturnSpeedRect, property.FindPropertyRelative("speed"), GUIContent.none);
                break;

            case MoveType.LOOK_CHAIN:
                // show waypoint.startPoint;
                Rect lookChainStartRect = new Rect(position.x, position.y + 30, position.width, 18f);
                EditorGUI.PropertyField(lookChainStartRect, startPoint, true);
                // show waypoint.endPoint;
                Rect lookChainEndRect = new Rect(position.x, position.y + 50, position.width, 18f);
                EditorGUI.PropertyField(lookChainEndRect, endPoint, true);
                //show speed
                Rect lookChainSpeedRect = new Rect(position.x, position.y + 70, 70, position.height);
                EditorGUI.PropertyField(lookChainSpeedRect, property.FindPropertyRelative("speed"), GUIContent.none);
                // look points?
                Rect lookChainTargetRect = new Rect(position.x, position.y + 90, position.width, 18f);
                EditorGUI.PropertyField(lookChainTargetRect, lookTarget, true);

                break;


        }

        EditorGUI.EndProperty();
    }

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        return base.GetPropertyHeight(property, label) + extraHeight;
    }

}


