﻿using UnityEngine;
using System.Collections;
using UnityEditor;



[CustomPropertyDrawer(typeof(Facings))]
public class FacingsDrawer : PropertyDrawer
{
    Facings thisFacings;
    float extraHeight = 75f;
    float extraHeight_CONST = 75f;
    float LINE_HEIGHT = 18f;
    float INDENT = 22f;

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        EditorGUI.BeginProperty(position, label, property);

        //setup a position to display the variable
        Rect facingPos = new Rect(position.x + INDENT, position.y + LINE_HEIGHT, position.width - INDENT, LINE_HEIGHT);
        //get the variable off the script
        SerializedProperty facingType = property.FindPropertyRelative("facingTypes");
        //Display the property in the editor
        EditorGUI.PropertyField(facingPos, facingType);

        switch ((FacingType)facingType.enumValueIndex)
        {
            #region "Facing"
            case FacingType.FREE_MOVEMENT:
                Rect freelookPos = new Rect(position.x + 2 * INDENT, position.y + 2 * LINE_HEIGHT, position.width - 2 * INDENT, LINE_HEIGHT);
                SerializedProperty freelookTime = property.FindPropertyRelative("freeLookTime");
                EditorGUI.PropertyField(freelookPos, freelookTime);
                break;
            case FacingType.LOOK_AND_RETURN:
                Rect lookDestinationPos = new Rect(position.x + 2 * INDENT, position.y + 2 * LINE_HEIGHT, position.width - 2 * INDENT, LINE_HEIGHT);
                Rect waitTimePos = new Rect(position.x + 2 * INDENT, position.y + 3 * LINE_HEIGHT, position.width - 2 * INDENT, LINE_HEIGHT);

                Rect travelToLabelPos = new Rect(position.x + 3 * INDENT, position.y + 4 * LINE_HEIGHT, (position.width - 3 * INDENT) * 0.2f, LINE_HEIGHT);
                Rect travelToPos = new Rect(position.x + 3 * INDENT + (position.width - 3 * INDENT) * 0.2f, position.y + 4 * LINE_HEIGHT, (position.width - 3 * INDENT) * 0.3f, LINE_HEIGHT);
                Rect travelFromLabelPos = new Rect(position.x + 3 * INDENT + (position.width - 3 * INDENT) * 0.5f, position.y + 4 * LINE_HEIGHT, (position.width - 3 * INDENT) * 0.2f, LINE_HEIGHT);
                Rect travelFromPos = new Rect(position.x + 3 * INDENT + (position.width - 3 * INDENT) * 0.7f, position.y + 4 * LINE_HEIGHT, (position.width - 3 * INDENT) * 0.3f, LINE_HEIGHT);

                SerializedProperty lookDestination = property.FindPropertyRelative("lookDestination");
                SerializedProperty lookwait = property.FindPropertyRelative("lookTime");
                SerializedProperty travelTo = property.FindPropertyRelative("travelToTime");
                SerializedProperty travelFrom = property.FindPropertyRelative("travelBackTime");

                EditorGUI.PropertyField(lookDestinationPos, lookDestination);
                EditorGUI.PropertyField(waitTimePos, lookwait);
                EditorGUI.LabelField(travelToLabelPos, "Time to: ");
                EditorGUI.LabelField(travelFromLabelPos, "Time back: ");
                EditorGUI.PropertyField(travelToPos, travelTo, GUIContent.none);
                EditorGUI.PropertyField(travelFromPos, travelFrom, GUIContent.none);

                break;
            case FacingType.LOOK_CHAIN:
                break;
            case FacingType.FORCED_LOCATION:
                Rect forcedDestinationPos = new Rect();
                Rect forcedTimePos = new Rect(position.x + 2 * INDENT, position.y + 3 * LINE_HEIGHT, position.width - 2 * INDENT, LINE_HEIGHT);

                SerializedProperty forcedLookDestination = property.FindPropertyRelative("forcedLookTarget");
                SerializedProperty forcedWait = property.FindPropertyRelative("forcedLookTime");

                EditorGUI.PropertyField(forcedDestinationPos, forcedLookDestination);
                EditorGUI.PropertyField(forcedTimePos, forcedWait);
                break;
                #endregion
        }


        EditorGUI.EndProperty();
    }

}
