﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(Engine))]
public class EngineEditor : Editor
{
    Engine engineScript;

    void Awake()
    {
        engineScript = (Engine)target;
    }

    public override void OnInspectorGUI()
    {
        //we are working with an array so we need a serializedObject
        //update and SerializedObjectApplyModifiedProperties
        serializedObject.Update();
     
        SerializedProperty controller = serializedObject.FindProperty("waypointController");
        EditorGUILayout.PropertyField(controller);

        if (controller.isExpanded)
        {
            //Shows the size of the array
            EditorGUILayout.PropertyField(controller.FindPropertyRelative("Array.size"));

            //indents to look more put together
            EditorGUI.indentLevel++;

            for (int i = 0; i < controller.arraySize; i++)
            {
                EditorGUILayout.PropertyField(controller.GetArrayElementAtIndex(i));
            }

            EditorGUI.indentLevel--;
        }
        serializedObject.ApplyModifiedProperties();
    }
}
