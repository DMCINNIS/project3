﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomPropertyDrawer(typeof(Waypoints))]
public class WaypointDrawer : PropertyDrawer
{
    Waypoints thisObject;
    float extraHeight = 55.0f;

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        EditorGUI.BeginProperty(position, label, property);

        //Serialized properties
        SerializedProperty waypointType = property.FindPropertyRelative("waypointType");

        //Rects for placement
        Rect waypointTypeDisplay = new Rect(position.x, position.y, position.width, 15.0f);

        //Displaying properties
        EditorGUI.PropertyField(waypointTypeDisplay, waypointType, GUIContent.none);


       switch((Type)waypointType.enumValueIndex)
        {
            case Type.MOVEMENT:
                Rect moveRect = new Rect(position.x, position.y, position.width, 15.0f);
                EditorGUI.PropertyField(moveRect, waypointType);
                break;
            case Type.FACINGS:
                Rect faceRect = new Rect(position.x, position.y, position.width, 15.0f);
                EditorGUI.PropertyField(faceRect, waypointType);
                break;
            case Type.EFFECTS:

                break;
        }
        EditorGUI.EndProperty();
    }

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        return base.GetPropertyHeight(property, label) + extraHeight;
    }
}
