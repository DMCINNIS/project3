﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomPropertyDrawer(typeof(Effects))]
public class EffectsDrawer : Effects
{
    public override void OnInspectorGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        EditorGUI.BeginProperty(position, label, property);

        //Serialized properties
        SerializedProperty effectType = property.FindPropertyRelative("effectsType");

        //Rects for placement
        Rect waypointTypeDisplay = new Rect(position.x, position.y, position.width, 15.0f);

        //Displaying properties
        EditorGUI.PropertyField(waypointTypeDisplay, effectType, GUIContent.none);


        switch ((EffectTypes)effectType.enumValueIndex)
        {
            case EffectTypes.CAMERA_SHAKE:
                Rect shakeRect = new Rect(position.x, position.y, position.width, 15.0f);
                EditorGUI.PropertyField(shakeRect, effectType);
                break;
            case EffectTypes.FADE:
                Rect fadeRect = new Rect(position.x, position.y, position.width, 15.0f);
                EditorGUI.PropertyField(fadeRect, effectType);
                break;
            case EffectTypes.SPLATTER:
                Rect splatRect = new Rect(position.x, position.y, position.width, 15.0f);
                EditorGUI.PropertyField(splatRect, effectType);
                break;
        }
        EditorGUI.EndProperty();
    }

}
