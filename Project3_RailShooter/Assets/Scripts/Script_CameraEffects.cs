﻿using UnityEngine;
using System.Collections;

public class Script_CameraEffects : MonoBehaviour
{
    public enum EffectType{ CAMERA_SHAKE, CAMERA_SPLATTER, CAMERA_FADE}

    private bool isCameraShaking = false;
    public bool IsCameraShaking
    {
        get { return isCameraShaking; }
        set { isCameraShaking = value; }
    }

    IEnumerator ShakeCamera(float duration, float intensity)
    {
        float startTime = Time.time;

        while (Time.time < startTime + duration)
        {
            Camera.main.transform.localPosition = Random.insideUnitSphere * intensity;
            yield return new WaitForEndOfFrame();
        }
    }

    IEnumerator Splatter(float splatterDuration, bool doesSplatterFade)
    {
        float startTime = Time.time;

        while(Time.time < startTime + splatterDuration)
        {
            //instaniate the splatter object
            if (doesSplatterFade == true)
            {
                float splatterFadeDuration;
                //if(splatterFadeDuration > 1)
                    //fade splatter object
                //Destroy(gameObject.Splatter);
            }
            yield return new WaitForEndOfFrame();
        }
        //Destroy(gameObject.Splatter);
    }

    IEnumerator FadeCamera(float fadeDuration)
    {
        for (float i = 0; i < fadeDuration; fadeDuration--)
        {
            //Camera.main.transform.renderer
            yield return new WaitForEndOfFrame();
        }
    }


}
