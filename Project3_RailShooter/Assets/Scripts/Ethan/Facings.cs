﻿using UnityEngine;
using System.Collections;
using UnityEditor;

public enum FacingType
{
    FREE_MOVEMENT,
    LOOK_AND_RETURN,
    LOOK_CHAIN,
    FORCED_LOCATION
}
public class Facings : Waypoints
{

    /*
    all the stuff going into engine

    if (freeLook)
        {
            MouseX = Input.GetAxis("MouseX");
            MouseY = Input.GetAxis("MouseY"); 


        }
    
    /// <summary>
    /// first facings type, free looks the camera
    /// </summary>
    /// <returns></returns>
    IEnumerator FreeMovement()
    {
        //start free look
        freeLook = true;

        //placeholder yield
        yield return null;
    }

    /// <summary>
    /// second facings type, looks at a destination and then returns to previous location
    /// </summary>
    /// <returns></returns>
    IEnumerator LookAndReturn()
    {
        freeLook = false;
        //placeholder yield
        yield return null;
    }

    /// <summary>
    /// third facings type, looks at a list of targets one after another for periods of time
    /// </summary>
    /// <returns></returns>
    IEnumerator LookChain()
    {
        freeLook = false;
        //placeholder yield
        yield return null;
    }

    /// <summary>
    /// fourth facings type, looks at a target
    /// </summary>
    /// <returns></returns>
    IEnumerator ForcedLocation()
    {
        freeLook = false;
        //lookatme() on target

        //placeholder yield
        yield return null;
    }
    */
}