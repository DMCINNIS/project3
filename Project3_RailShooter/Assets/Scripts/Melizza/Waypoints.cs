﻿using UnityEngine;
using System.Collections;


public enum Type
{
    MOVEMENT = 1,
    FACINGS,
    EFFECTS
}

public enum MoveType
{
    STRAIGHT_LINE = 1,
    BEZIER_CURVE,
    WAIT,
    LOOK_RETURN,
    LOOK_CHAIN
}

public enum FacingTypes
{
    FREE_MOVEMENT = 1,
    LOOK_RETURN,
    LOOK_CHAIN,
    FORCED_LOCATION
}

public enum EffectTypes
{
    CAMERA_SHAKE = 1,
    SPLATTER,
    FADE
}

[System.Serializable]
public class Waypoints
{
    public Type effectType;
    public Type facingType;
    public Type moveType;


    
    public int speed;
    public int waitTime;
    public Transform startPoint;
    public Transform endPoint;
    public Transform bezierCurve;
    public Transform lookTarget;
    public Transform[] chainTransformers;

    #region "Facing"
    public FacingTypes FacingType;

    //free movement
    public float freeLookTime;

    //look and return
    public Transform lookDestination;
    public float lookTime;
    public float travelToTime;
    public float travelBackTime;

    //look chain
    public Transform[] lookPoints;
    public float[] lookTimes;

    //forced location
    public Transform forcedLookTarget;
    public float forcedLookTime;

    #endregion
}
