﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using UnityEngine.UI;


public class Engine : MonoBehaviour
{
    public Waypoints[] waypointController;
    public Image splatter;
    public GameObject splatGO;
    [Tooltip("How many seconds the type of Look, facing or effect lasts")]
    public float secondsItLasts;
    public float shakeAmount;
    public float shakePercentage;
    public float shakeDuration;
    public float startDuration;


    IEnumerator Start()
    {
        splatGO = GameObject.Find("Cube");
        splatter = splatGO.GetComponent<Image>();
        for (int i = 0; i < waypointController.Length; i++)
        {
            if (waypointController[i].effectType == Type.MOVEMENT)
            {

                //now that we are in movement waypoint
                if (waypointController[i].moveType == (Type)MoveType.BEZIER_CURVE)
                {
                    StartCoroutine(BezierCurve());
                }
                else if (waypointController[i].moveType == (Type)MoveType.STRAIGHT_LINE)
                {
                    StartCoroutine(StraightLine());
                }
                else if (waypointController[i].moveType == (Type)MoveType.WAIT)
                {
                    StartCoroutine(Wait());
                }
                else if (waypointController[i].moveType == (Type)MoveType.LOOK_RETURN)
                {
                    StartCoroutine(LookReturn());
                }
                else if (waypointController[i].moveType == (Type)MoveType.LOOK_CHAIN)
                {
                    StartCoroutine(MovementLookChain());
                }

                //know that we are in movement waypoint

                //if (waypointController[i].moveType == MoveType.BEZIER_CURVE)
                //{
                //yield return new StartCoroutine(BezierCurve);
                //}
                //else if(wayp)
            }

            if (waypointController[i].effectType == Type.FACINGS)
            {
                yield return StartCoroutine("FacingEngine", i);
            }


            if (waypointController[i].effectType == Type.EFFECTS)
            {
                if (waypointController[i].effectType == (Type)EffectTypes.CAMERA_SHAKE)
                {
                    StartCoroutine(CameraShake());
                }
                else if (waypointController[i].effectType == (Type)EffectTypes.FADE)
                {
                    StartCoroutine(Fade());
                }
                else if (waypointController[i].effectType == (Type)EffectTypes.SPLATTER)
                {
                    StartCoroutine(Splatter());
                }
            }
        }
    }

    IEnumerator BezierCurve()
    {
        yield return new WaitForSeconds(secondsItLasts);
    }

    IEnumerator StraightLine()
    {
        yield return new WaitForSeconds(secondsItLasts);
    }

    IEnumerator Wait()
    {
        yield return new WaitForSeconds(secondsItLasts);
    }

    IEnumerator LookReturn()
    {
        yield return new WaitForSeconds(secondsItLasts);
    }

    IEnumerator MovementLookChain()
    {
        yield return new WaitForSeconds(secondsItLasts);
    }


    //effect IEnumerators
    IEnumerator CameraShake()
    {
        Vector3 rotationAmount = Random.insideUnitSphere * shakeAmount;
        rotationAmount.z = 0;

        shakePercentage = shakeDuration* startDuration;
        shakeDuration = Mathf.Lerp(shakeDuration, 0, Time.deltaTime);

        yield return new WaitForSeconds(secondsItLasts);
    }
    IEnumerator Fade()
    {

        yield return new WaitForSeconds(secondsItLasts);
    }
    IEnumerator Splatter()
    {
        splatter.CrossFadeAlpha(0.5f, secondsItLasts, true);
        yield return new WaitForSeconds(secondsItLasts);
    }

    IEnumerator FacingsEngine(int index)
    {
        switch (waypointController[index].FacingType)
        {
            case FacingTypes.FREE_MOVEMENT:
                //this one just happens with mouseLook always being active unless I switch it off in here
                break;
            case FacingTypes.LOOK_RETURN:
                //not doing this one
                GetComponent<MouseLook>().enabled = false;
                break;
            case FacingTypes.LOOK_CHAIN:
                //not doing this one
                GetComponent<MouseLook>().enabled = false;
                break;
            case FacingTypes.FORCED_LOCATION:
                GetComponent<MouseLook>().enabled = false;
                Transform target = waypointController[index].forcedLookTarget;
                float forcedTime = waypointController[index].forcedLookTime;
                Quaternion start = transform.rotation;
                Quaternion targetDir = Quaternion.LookRotation(target.position - transform.position);
                float step = 0.0f;
                //lerp rotations
                while (step < 1.0f)
                {
                    Quaternion.Lerp(start, targetDir, step);
                    step += Time.deltaTime;
                    yield return new WaitForEndOfFrame();
                }
                //wait at target
                yield return new WaitForEndOfFrame();
                break;
        }
        yield return null;
    }
}